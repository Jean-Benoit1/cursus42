#include "../ft_lib.h"

void	ft_bzero(char *str, int n)
{
	ft_memset(str, '\0', n);
}
