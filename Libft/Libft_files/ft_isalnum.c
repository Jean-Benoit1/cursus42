#include "../ft_lib.h"

int	ft_isalnum(int character)
{
	if (ft_isdigit(character) == 1 || ft_isalpha(character) == 1)
		return (1);
	return (0);
}
