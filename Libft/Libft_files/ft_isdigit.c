int	ft_isdigit(int character)
{
	if (character >= 48 && character <= 57)
		return (1);
	return (0);
}
