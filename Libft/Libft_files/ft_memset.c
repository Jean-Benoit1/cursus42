#include "../ft_lib.h"

void	*ft_memset(char *str, char c, int n)
{
	int	strlen;
	int	i;

	strlen = ft_strlen(str);
	if (strlen < n)
		return ("error");
	i = 0;
	while (i < n)
	{
		str[i] = c;
		i++;
	}
	return (str);
}
