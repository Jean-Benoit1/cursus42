int ft_strlen(char *str);
int ft_isalpha(int character);
int ft_isdigit(int character);
int ft_isalnum(int character);
void *ft_memset(char *str, char c, int n);
void ft_bzero(char *str, int n);
