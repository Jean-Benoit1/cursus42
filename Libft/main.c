#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include "ft_lib.h"

void call_no_param();
void call_memset(char *str, char c, int n);
void call_bzero(char *str, int n);

int main()
{
    char str_memset[] = "this is a test for memset";
    char c = 'c';
    int n = 3;
    char str_bzero[] = "this is a test for bzero";
    call_bzero(str_bzero, n);
    call_memset(str_memset, c, n);
    call_no_param();
}

void call_no_param()
{
    printf("Fonction strlen : %i\n", ft_strlen("j'aime les chats"));
    printf("Fonction isAlpha de ctype : %i\n", isalpha('%'));
    printf("Fonction isAlpha de mon programme : %i\n", ft_isalpha('%'));
    printf("Fonction isDigit de ctype : %i\n", isdigit('6'));
    printf("Fonction isDigit de mon programme : %i\n", ft_isdigit('6'));
    printf("Fonction isAlnum de ctype : %i\n", isalnum('%'));
    printf("Fonction isAlnum de mon programme : %i\n", ft_isalnum('%'));
}

void call_memset(char *str, char c, int n)
{
    char *str1 = str;
    char *output = memset(str, c, n);
    char *outputme = ft_memset(str1, c, n);
    printf("Fonction memeset de string : %s\n", output);
    printf("Fonction memset de mon programme : %s\n", outputme);
}

void call_bzero(char *str, int n)
{
    char *str1 = str;
    bzero(str, n);
    ft_bzero(str1, n);
    printf("Fonction bzero de string : %s\n", str);
    printf("Fonction bezro de mon programme : %s\n", str1);
}
